/*******************************************************************************
 * Copyright (C): 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-CLI <https://www.jmo-lang.org>.
 *
 * JayMo-CLI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-CLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-CLI. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.cli.info;

import java.io.InputStream;

import de.mn77.base.error.Err;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 06.05.2021
 */
public class Lib_CliInfo {

	public static Object help() {

		try {
			final InputStream is = Lib_Jar.getStream( "/org/jaymo_lang/cli/info/help.txt" );
			return Lib_Stream.readUTF8( is );
		}
		catch( final Exception e ) {
			Err.show( e );
			return "!! Can't read help file !!";
		}
	}

	public static String license() {

		try {
			final InputStream is = Lib_Jar.getStream( "/org/jaymo_lang/cli/info/license.txt" );
			return Lib_Stream.readUTF8( is );
		}
		catch( final Exception e ) {
			Err.show( e );
			return "!! Can't read license file !!";
		}
	}

}
